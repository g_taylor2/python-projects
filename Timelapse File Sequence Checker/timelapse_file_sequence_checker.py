# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 16:18:05 2019

@author: graha
"""


import os																						
import os.path																					
from pathlib import Path																		

# Define file directory path to search for present and missing image(s)
directory = 'J:\\Graham\\Biocarbon Engineering\\Data\\Lab Data\\Time-Lapse_Tent\\Combined\\'	

# Define start variable integer 'j'
j = 11218																						

# Iterate over every file in "Directory" path																								
for file in os.listdir(directory):	
    # Create new "file_name" variable based on file number searched											
    increment = 'G00%d.JPG' % j
    file_name = os.path.join(directory, increment) 	
    # Outout file name for each file											 
    print(file_name)																			
	# Define new variable "my_file" 
	my_file = Path(file_name)																	
    try:
        # Check if file exists in path										
        my_abs_path = my_file.resolve()
        # Create/ammend a new text file "search.txt" in current directory and 
        with open ('search.txt','a') as file_:	
        # List missing file and accompany with the text "File found"												
            file_.write(increment + "\tFile found\n")		      												
    except FileNotFoundError:	
        # Create/ammend a new text file "search.txt" in current directory and 																
        with open ('search.txt','a') as file_:					
            # List missing file and accompany with the text "File does not exist"								
            file_.write(increment + "\tFile does not exist\n")																											
        								
    # Increment count variable integer j by 1 and repeat until files in directory parsed
    j += 1																						