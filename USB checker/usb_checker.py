# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 11:25:30 2019

@author: graha
"""

import win32com.client
# Outputs USB devices connected to usb ports on device
wmi = win32com.client.GetObject ("winmgmts:")
for usb in wmi.InstancesOf ("Win32_USBHub"):
    print(usb.DeviceID)