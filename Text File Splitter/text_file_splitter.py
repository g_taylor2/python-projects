# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 20:14:50 2019

@author: Graham Taylor
"""
# Declare desired lines in each split file
lines_per_file = 100000
# Open large text file to be split as "large_file"
with open('E:\\House Price Data\\pp-complete.txt') as large_file:
    # Count and iterate over each line in large_file
    for line_number, line in enumerate(large_file):
        # Check if line number and lines per file equals 0 using remainder
        if line_number % lines_per_file == 0:
            # Define name of each split file based on line_number
            split_file_name = 'E:\\House Price Data\\House_Price_{}.txt'.format(line_number)
            # Prepare split file for write and open
            split_file = open(split_file_name, "w")       
        # Line number and lines per file are not equal because remainder is different from 0
        else:
            # Write each line to split file
            split_file.write(line)