# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 15:36:25 2019

@author: graha
"""

import win32com.client
import os

# Assign initial value to count variable
i = 0
# Select input path directory folder of .xlsb files to convert
in_path = "C:\\Users\\graha\\Desktop\\Temp"
# Select output path folder of .xlsx files to convert
out_path = "C:\\Users\\graha\\Desktop\\Output\\Book1.xlsx"
# For every file in the input folder directory    
for file in os.listdir(in_path):  
    # If the file ends with the file extension .xlsb, continue here
    if file.endswith(".xlsb"):
        files = os.listdir(in_path)
        # Create a list exclusively of .xlsb files
        files_xlsb = [i for i in files if i.endswith('.xlsb')]
        # Replace file extension .xlsb with null for each input file in folder
        files_xlsb_tidy = [item.replace(".xlsb","") for item in files_xlsb]
        print(files_xlsb)
        # Generate source file name to process using file name from source directory and source directory path
        name = os.path.join(in_path, file)
        print(name)
        # Generate processed file name using file name from source directory and source directory path
        name2 = files_xlsb_tidy[i]
        # Launch Excel
        excel = win32com.client.Dispatch("Excel.Application")
        # Supress display alert warnings upon file opening
        excel.DisplayAlerts = False
        # Make Excel workbook invisible
        excel.Visible=False
        doc = excel.Workbooks.Open(name)
        # Create a new file name by joining file destination path with no extension
        file_name = os.path.join(out_path, name2)
        print(file_name)
        # Save file with .xlsx extension
        doc.SaveAs(Filename=file_name,FileFormat=51)
        # Close the document once save completed
        doc.Close()
        # Quit Excel
        excel.Quit()      
        # Increment list to handle and continue until all files converted
        i += 1