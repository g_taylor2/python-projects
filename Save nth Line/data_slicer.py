# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 10:59:50 2019

@author: graha
"""

# Source .txt file that needs size to be reduced by removing lines of data
with open("I:\\PhD 2011-2015\\Data\\Graham2\\160330\\160330recorder2file53.txt", 'r') as f:
    # Iterate over each line, start at line because line 1 has headers
    for count, line in enumerate(f, start=1):
        # Slice out data from source file between specified lines
        if 1279766 < count < 1349235:
            # Optional output of current line in code that is removed
            print(line)
            # Create .txt file with reduced data to produce scientific figures focused on sliced data only
            with open("C:\\Users\\graha\\Desktop\\test_160330_full.txt", 'a') as file:
                file.write(line)
                
                