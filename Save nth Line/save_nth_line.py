# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 10:53:51 2019

@author: graha
"""
# Source .txt file that needs size to be reduced by removing lines of data
with open("I:\\PhD 2011-2015\\Data\\Graham2\\140407\\140407recorder2file19.txt", 'r') as f:
    # Iterate over each line, start at line because line 1 has headers
    for count, line in enumerate(f, start=1):
        # Output and save every 10th line from file
        if count % 10 == 0:
            # Optional output of current line in code that is removed
            print(line)
            # Create .txt file with reduced data for faster processing on slower machines
            with open("C:\\Users\\graha\\Desktop\\test_140407_full.txt", 'a') as file:
                file.write(line)
                