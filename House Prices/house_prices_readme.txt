Webscraping tool written in Python 3.x Property Prices sourced from Right Move (https://www.rightmove.co.uk/) for a given geographical loction in the UK. Outputs "cleaned" data to include:

1) Property Type (e.g. House/Flat/Bungalow/Apartment etc.) 
2) Number of Bedrooms
3) Street Number (if known)
4) Listed Price in GBP
5) Geographical coordinates of Property Location