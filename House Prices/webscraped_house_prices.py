# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 17:06:05 2019

@author: graha
"""

from bs4 import BeautifulSoup
from urllib.parse import urljoin
import requests, bs4, urllib, re
import numpy as np
import urllib.request as urllib2
import datetime 
                                                       
i = 0                                                                        
HEADERS = {'User-Agent': 'Mozilla/6.0'}
proxy = {'http': 'http://www.rightmove.co.uk/.html'}

# total number of regions = 27761; region 1473 is Witney, England  
region = 1473
# enter search radius in miles as a decimal e.g. 3.0 for area of interest
miles_radius = 0.0 
# enter here desired max property price in GBP
max_price = 20000000
# enter here max number of property bedrooms
max_bedrooms = 10
# define base URL
base_url = 'http://www.rightmove.co.uk/property-for-sale/find.html?'
# add additional arguments to URL string
addition = '?locationIdentifier=REGION^' + str(region) + '&maxBedrooms=' + str(max_bedrooms) + '&maxPrice=' + str(max_price) + '&radius=' + str(miles_radius) + '&includeSSTC=false'
# combine sub-strings to produce sngle custom url
url = urllib.parse.urljoin(base_url, addition)

# output file located in User Graham Taylor
# add headings to text file data columns
def headings():
    with open('House_Prices2.txt', 'a') as the_file:
        the_file.write("DATE RETRIEVED")
        the_file.write('\t')
        the_file.write("TIME RETRIEVED")
        the_file.write('\t')
        the_file.write("TOWN/CITY")
        the_file.write('\t')
        the_file.write("COUNTY")
        the_file.write('\t')
        the_file.write("SUBURB/STREET")
        the_file.write('\t')
        the_file.write("TYPE")
        the_file.write('\t')
        the_file.write("NUMBER OF BEDROOMS")
        the_file.write('\t')
        the_file.write("PRICE (GBP)")
        the_file.write('\t')
        the_file.write("LATITUDE")
        the_file.write('\t')
        the_file.write("LONGITUDE")
        the_file.write('\n')       
    
def get_property_soup():
    response = requests.get(url, headers=HEADERS)
    return BeautifulSoup(response.content, 'lxml')

def get_property_soup2():
    response2 = requests.get(url2, headers=HEADERS)
    return BeautifulSoup(response2.content, 'lxml')

def get_location():
    global soup
    soup = get_property_soup()
    
    # get title of page of property list that described general search location on webpage
    location = soup.select('h1.searchTitle-heading')[0].text.strip()
    # output property location
    # print(location)
    # shorten location component of title by slicing data from the word "in"                                                                  
    start_location = location.find('in')
    # shorten location component of title by slicing data before the word "within"   
    end_location = location.find('within', start_location)                                               
    # get village/town/city name and county                                                                         
    extract_location = location[start_location:end_location]
    # output village/town/city location and county
    print(extract_location)
    # remove the word "in" from location string
    global tidy_location1
    tidy_location1 = extract_location.replace("in ","")
    # split the location string into village/town/city and county based on comma
    global tidy_location2
    tidy_location2 = tidy_location1.split(',')
    
    global listed
    listed = []
    for loc in tidy_location2:
        locs = loc.lstrip(' ')
        listed.append(locs) 
    
    # output location as list
    # print(tidy_location2) 
    # establish the number of preperties listed on webpage. Maximum total per page by default is 25.
    number_of_properties_on_page = len(soup.findAll('address', class_ ='propertyCard-address'))
    # output total number of properties listed on webpage
    print(number_of_properties_on_page)
    # create a list of properties based on the number of properties listed on webpage
    # properties = [i for i in range(number_of_properties_on_page)]
    # get details of each property in soup
    property_header_details = soup.findAll('a', class_= 'propertyCard-headerLink')#[i].text.strip()
    # print(property_header_details)
    # convert soup of property details to string
    str_property_header_details = str(property_header_details)
    # get the unique identifier url address of each property on property list webpage
    global url_property_numbers
    url_property_numbers = re.findall(r'\d+', str_property_header_details)
    
def write_up():
    # optional output of specific property webpage title
    print(title)
    # optional output string of number of bedrooms at property
    print(title[0])
    # optional output string of property title string without number of bedrooms
    title2 = title[1:]
    print(str(title2))
    # shorten 'text' variable to 't' for brevity
    t = str(title2)   
    # start data slice from this defined point
    start_title= t.find('bedroom')
    # end data slice from this defined point
    end_title = t.find('sale', start_title)                                              
    t[start_title:end_title]                                                                            #define list
    extract_title = t[start_title:end_title]                                                             #place list contents into variable 'extract_line'
    print(extract_title)
    # get property type from title by:
    # removing "bedroom" from string
    extract_title2 = extract_title.replace('bedroom', '')
    # removing "for" from string
    extract_title3 = extract_title2.replace('for', '')
    # removing whitespace from string
    property_type = extract_title3.strip()
    print(property_type)
    
    # extract_title5 = extract_title4.split(' ')
    #File located in User Graham Taylor
    with open('House_Prices2.txt', 'a') as the_file:
        current_datetime = datetime.datetime.now()
        current_datetime_string = str(current_datetime)
        current_datetime_split = current_datetime_string.split(' ')
        the_file.write(current_datetime_split[0])
        the_file.write('\t')
        the_file.write(current_datetime_split[1])
        the_file.write('\t')
        the_file.write(listed[0])
        the_file.write('\t')
        the_file.write(listed[1])
        the_file.write('\t')
        the_file.write(property_locations[0])
        the_file.write('\t')
        the_file.write(property_type)
        the_file.write('\t')
        the_file.write(title[0])
        the_file.write('\t')
        
        try:
            the_file.write(property_price_as_list[0])
        except IndexError:    
            the_file.write("POA")
            the_file.write('\n')
        
        if property_price_as_list[0]:
            try:
                the_file.write(property_price_as_list[1])
            except IndexError:    
                the_file.write("000")
                the_file.write('\t')
        else:
            print("Property price not found")
        
        if property_price_as_list[1]:
            try:
                the_file.write(property_price_as_list[2])
                the_file.write('\t')
            except IndexError:
                the_file.write('\t')
        else:
            the_file.write('\t')
            print("Property price not found")
        
        try:
            the_file.write(geo_numbers[1])
            the_file.write('\t')
        except IndexError:
            the_file.write('\t')
            
        try:
            the_file.write(geo_numbers[2])
            the_file.write('\t')
            the_file.write('\n')
        except IndexError:
            the_file.write('\t')    
            the_file.write('\n')
        
headings()

for region in range(1, 27761):
    try:
        get_location()
        results = soup.findAll('span', class_="searchHeader-resultCount")[0].text.strip()   
        # print(results)
        integer_results = int(results)
        number_of_webpages = int(integer_results / 25) + (integer_results % 25 > 0)
        print(number_of_webpages)
        number_of_index_webpages = number_of_webpages - 1

        number_of_properties_on_page = len(soup.findAll('address', class_ ='propertyCard-address'))
        number_of_properties_on_page_minus_one = number_of_properties_on_page - 1
        properties = [j for j in range(number_of_properties_on_page)]

        indexes = np.linspace(0,number_of_properties_on_page_minus_one*number_of_index_webpages,number_of_webpages).tolist()
        indexes_int = [int(e) for e in indexes]
        
        #for page, index in zip(range(number_of_webpages), indexes):
        for page in range(number_of_webpages):
            if page == 0:
                addition = '?locationIdentifier=REGION^' + str(region) + '&maxBedrooms=' + str(max_bedrooms) + '&maxPrice=' + str(max_price) + '&radius=' + str(miles_radius) + '&includeSSTC=false'
                # combine sub-strings to produce sngle custom url 
                url = urllib.parse.urljoin(base_url, addition)
                # indicate full url to user
                print(url)
            elif page >= 1:
                if i >= 24:
                    i = 0
                else:
                    i = i
                addition = '?locationIdentifier=REGION^' + str(region) + '&maxBedrooms=' + str(max_bedrooms) + '&maxPrice=' + str(max_price) + '&radius=' + str(miles_radius) + '&index=' + str(indexes_int[page]) + '&includeSSTC=false'
                # combine sub-strings to produce sngle custom url 
                url = urllib.parse.urljoin(base_url, addition)
                # indicate full url to user
                print(url) 
            # get details for each property on webpage

            for property_item in properties:
                try:
                    # get street and suburb details for each property
                    property_street_details = soup.findAll('address', class_='propertyCard-address')[i].text.strip()
                    # output street property details
                    # print(property_street_details)
                    tidy_property_street_details = property_street_details.replace(tidy_location2[2],"")
                    # separate street property details into street and suburb based on comma
                    property_locations = property_street_details.split(",")
                    with open("House_Prices2.txt", 'a') as infile:
                        # check if tidy_property_street_details is not an empty list
                        if tidy_property_street_details:
                            print(tidy_property_street_details)#, file=infile)
                        # tidy_property_street_details is an empty list, so print street address "not found"    
                        else:
                            print("END")#, file=infile)
                    # output street property locations
                    print(property_locations)    
                    # get property price of each property in GBP
                    property_price = soup.findAll('a', class_='propertyCard-priceLink propertyCard-salePrice')[i].text.strip()
                    # output property price in GBP
                    # print(property_price)
                    # price is written with a comma separator, split based on comma
                    property_price_as_list = re.findall(r"[-+]?\d*\.*\d+", property_price)
                    # output property price as a list
                    print(property_price_as_list)   
                # property_details2 = soup.findAll('a', class_='propertyCard-header')#[i].text.strip()
                # print(property_details2)
                    # get individual property url by combining baseline url and unique url arguments for each property
                    individual_property_url = 'https://www.rightmove.co.uk/property-for-sale/'         
                    individual_property_addition = 'property-' + str(url_property_numbers[i]) + '.html'
                    # create new url for each property webpage by joining url arguments
                    url2 = urllib.parse.urljoin(individual_property_url, individual_property_addition)                              
                    print(url2)
                    # get property information
                    soup2 = get_property_soup2()
                    location2 = soup2.findAll('a', class_='block js-tab-trigger js-ga-minimap')#[0].text.strip()
                    # print(location2)
                    str_location2 = str(location2)
                    geo_numbers = re.findall(r"[-+]?\d*\.*\d+", str_location2)
                    # output geocoordinates
                    print(geo_numbers)
                    # get property type information for property
                    title = soup2.select('title')[0].text.strip()
                    # call definition
                    get_location()
                    write_up()
                    i+=1
                except IndexError:
                    print("Moving on to next region")
                    i=0
                    
    except IndexError:
        print("IndexError")
        i=0