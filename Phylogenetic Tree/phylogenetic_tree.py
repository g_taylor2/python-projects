# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 17:14:42 2019

@author: graha
"""
# Generates Phylogenetic Tree for Hirschfeldia incana
from ete3 import Tree, NodeStyle, TreeStyle, AttrFace, faces
# Contruct Phylogenetic Tree
t = Tree("(((((((((((((((((((Capsella rubella)), ((Catolobus pendulus))),(((Camelina sativa))))))), (((((((Boechera canadensis))), (((Pachycladon fastigiatum)))))))), ((((((((Alyssopsis mollis)))), ((((Turnitis glabra))))), (((((Murbeckiella pinnatifida))))))))), (((((((((Arabidopsis lyrata))), (((Arabidopsis thaliana)))))))))), ((((((((((Physaria newberryii))))))))))), (((((((((((Erysimum cheiranthoides)), ((Erysimum cheiri)))))))))))), ((((((((((((Rorippa globosa, Rorippa sylvestris), (Rorippa indica))))), (((((Barbarea vulgaris))))))), (((((((Cardamine penysylvanica))))))))))))), (((((((((((((Smelowskia tibetica)))))))))))))), ((((((((((((((Lepidium campestre))))), (((((Lepidium didymum))))))))))))))), (((((((((((((((Macropodium nivale)))))))))))))))), ((((((((((((((((Hirschfeldia incana)))), ((((Raphanus raphanistrum)))))), ((((((Brassica rapa))))))), (((((((Brassica nigra)))))))), ((((((((Coincya monensis))))), (((((Sinapsis alba))))))))), (((((((((Cakile maritima)))))))))), ((((((((((Sisymbrium officinale))))))))))), (((((((((((Schrenkiella parvula)))))))))))), ((((((((((((Thlaspi alliaceum))))), (((((Thlaspi arvense)))))), ((((((Alliaria petiolata))))))))))), (((((((((((Eutrema salsugineum))))))))))))), (((((((((((((Calepina irregularis)))))))))))))), ((((((((((((((Noccaea caerulescens))))))))))))))), (((((((((((((((Kemera saxatilis)))))))))))))))), ((((((((((((((((Iberis gibraltarica))))), (((((Iberis sempervirens))))))))))))), (((((((((((((Lobularia maritima)))))))))))))), ((((((((((((((Cochlearia pyrenaica))))))))))))))), (((((((((((((((Biscutella cichoriifolia)))))))))))))), ((((((((((((((Lunaria annua)))))))))))))))), ((((((((((((((((Alyssum alyssoides))))))))), (((((((((Berteroa incana))))))))))))))))), (((((((((((((((((Diptychocarpus strictus)), ((Parrya nudicaulis)))), ((((Chlorispora bungeana))))))), (((((((Clausia aprica)))))))))))))))), ((((((((((((((((Bunias orientalis)))))))), ((((((((Hesperis matronalis)))))))))))))))))), ((((((((((((((((((Aethionema grandiflorum))))), (((((Aethionema subulatum)))))), ((((((Aethionema arabicum)))))))))))))))))));")
# Define style parameters of nodes
nstyle = NodeStyle()
nstyle["shape"] = "None"
nstyle["size"] = 0
nstyle["fgcolor"] = "black"

# Tree Style() used to render small trees used as leaf faces
small_ts = TreeStyle()
# Enables tree leaf names to be visible
small_ts.show_leaf_name = True
# Set scales
small_ts.scale = 10

# Define tree leaf node styles
def layout(node):
    if node.is_leaf():
        # Add node name to laef nodes and configure style
        N = AttrFace("name", fsize=100, fgcolor="black")
        faces.add_face_to_node(N, node, 0)

for n in t.traverse():
    n.set_style(nstyle)
 
# Output Phylogentic Tree as popup Window
t.show()
# Outut Phylogenetic Tree as .pdf
t.render("mytree2.pdf")