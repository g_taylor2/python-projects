# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 22:38:41 2019

@author: Graham Taylor
"""

import os, schedule, time

# Create a definition for first file to be run e.g. song
def task(time):
    # Specify directory of file source
    os.startfile('E:\\C an M drives\\D0132677_DATA\\Music\\Great Heart.mp3')
    print("Playing Now...",time)
    return

# Create a definition for another file to be run
def task2(time):
    # Specify directory of file source
    os.startfile('E:\\C an M drives\\D0132677_DATA\\Music\\Mango Groove-Island Boy.mp3')
    print("Playing Now...",time)
    return

# Schedule files to be run daily at specified times and output activity to user
schedule.every().day.at("22:37").do(task,'It is 22:37')
schedule.every().day.at("22:44").do(task2,'It is 22:44')

# Keep on running
while True:
    schedule.run_pending()
    # Optional sleep command
    time.sleep(0)